# Proyecto Final con react con firebase


#### Materia Tecnologia Web 1
#### Docente: jaime zambrana chacon

#### Alumnos: 
#### David Alpire Torrez
#### johannes machado paz

# Librerias Utilizadas


#Firebase
```
npm install firebase
```
-Con firebase usaremos la autenticacion para crear Usuarios y Firestore Para crear una base de datos de las lista de gasto
que añadiremos


styled Componentes
```
npm install --save styled-components
```
-Es una libreria de stylos css en react 

React-Helmet 
```
npm i react-helmet
```
Este componente React reutilizable administrará todos sus cambios en el encabezado del documento.

React router Dom
```
npm i react-router-dom@5.3.0
```
React date-picker y date-fns
```
npm install react-datepicker --save
```
```
npm install date-fns --save
```
Con estas 2 librerias estaremos creando un contenedor de calendario para selecionar la fecha en la q realizamos el gasto

## INSTALACION

###### Paso 1
 
 ```
 git clone  https://gitlab.com/alpiretorrezdavicho/proyecto-final-con-react-con-firebase.git

 ```
 ###### Paso 2

 Ubicar la carperta donde hicimos el git clone y ejecutar el siguiente comando

  ```
   npm install
   ```



