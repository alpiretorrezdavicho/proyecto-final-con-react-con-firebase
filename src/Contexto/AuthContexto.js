import React ,{useContext , useState,useEffect} from 'react'
import {auth} from '../Firebase/firebaseconfig'

//creamos el contexto
const AuthContext=React.createContext();

// hook para entrar al contexto
const useAuth= ()=>{

   return useContext(AuthContext);
}


const AuthProvider = ({children}) => {

    const [usuario , cambiarusuario]=useState();

    // creamos un state para saber cuando termina de cargar la comprobacion de onauthstatechanged

    const [cargando,cambiarcargando]=useState(true);

    //efecto para ejecutar una sola vez
    useEffect(()=>{
    //comprobamos si hay un usuario
    
    const cancelarsuscripcion= auth.onAuthStateChanged((usuario)=>{
        cambiarusuario(usuario); 
        cambiarcargando(false);    
    })
    return cancelarsuscripcion;
    },[])
    return ( 
        <AuthContext.Provider value={{usuario:usuario}}>
     {/*
     solamente retornamos los elementos hijos cuando este cargando.
     de esta forma nos aseguramos de no cargar el resto de la app hasta que el usuario
     hay sido establecido.
     si no hacemos esto al refrescar la pagina el componente children va intentar 
     cargar inmediatamente antes de haber comprobado que hay un usuario.

     
     */}
           { !cargando && children}

        </AuthContext.Provider>
      
     );
}
 
export {AuthProvider , AuthContext, useAuth};