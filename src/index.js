import React  from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import WebFont from  'webfontloader'
import Contenedor from './Elementos/contenedor'
import  { Route, BrowserRouter,Switch} from 'react-router-dom'
import  EditarGastos from './componentes/EditarGasto'
import  GastoPorCategorias from './componentes/GastosPorCategoria'
import  InicioSession from './componentes/InicioSesion'
import  ListaDEgastos from './componentes/ListadeGastos'
import   RegistrodeUsuario from './componentes/RegistroUsuario'
import {Helmet} from "react-helmet";
import favicon from "./imagenes/logo.png"
import Fondo from './Elementos/Fondo'
import {AuthProvider} from './Contexto/AuthContexto'
import RutaPrivada from './componentes/RutaPrivada'
import ListadeGastos from './componentes/ListadeGastos';
WebFont.load({
  google: {
    families: ['Work Sans:400,500,700', 'sans-serif']
  }
});



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
<>
  <Helmet>
<link rel="shortcut icon" href={favicon} type="image/x-icon"/>

</Helmet>

<AuthProvider>
<BrowserRouter>

<Contenedor>
    <Switch>
      <Route path='/iniciar-sesion' component={InicioSession}></Route>
      <Route path='/crear-cuenta' component={RegistrodeUsuario}></Route>

      <RutaPrivada path='/categoria'>
        <GastoPorCategorias/>
      </RutaPrivada>
      <RutaPrivada path='/lista'>
        <ListadeGastos/>
      </RutaPrivada>
      <RutaPrivada path='/editar/:id'>
        <EditarGastos/>
      </RutaPrivada>
      <RutaPrivada path='/'>
        <App/>
      </RutaPrivada>
      {/*
      <Route path='/lista' component={ListaDEgastos}></Route>
      <Route path='/editar-gastos' component={EditarGastos}></Route>
      <Route path='/' component={App}></Route>
      */ }
    </Switch> 
</Contenedor>
</BrowserRouter>

</AuthProvider>

  <Fondo/>

  </> 
);

