import {useEffect, useState} from 'react';
import {db} from './../Firebase/firebaseconfig'
import {useHistory} from 'react-router-dom';
import { useAuth } from '../Contexto/AuthContexto';

const useObtenerGasto = (id) => {
    const {usuario} = useAuth();
	const [gasto, CambiarGasto] = useState([]);
    const [ultimoGasto,cambiarUltimoGasto ]= useState(null)
    const [hayMasPorCargar,cambiarHayMasPorCargar ]= useState(false)

    const obtenerMasgastos=()=>{

        db.collection('gastos')
        .where('uidUsuario' , '==' , usuario.uid)
        .limit(5)
        .startAfter(ultimoGasto)
        .onSnapshot((snapshot)=>{
            if(snapshot.docs.length > 0){
                cambiarUltimoGasto(snapshot.docs[snapshot.docs.length-1]) 
                CambiarGasto( gasto.concat(snapshot.docs.map((gasto)=>{
                    return {...gasto.data(),id:gasto.id}
                 })))
            }else{
                cambiarHayMasPorCargar(false);
            }

        })
    }
	
	useEffect(() => {
		const unsuscribe= db.collection('gastos').where('uidUsuario' , '==' , usuario.uid)
        .limit(5)
        .onSnapshot((snapshot)=>{
            if(snapshot.docs.length>0){
            cambiarUltimoGasto(snapshot.docs[snapshot.docs.length-1])
            cambiarHayMasPorCargar(true)

            }else{
                cambiarHayMasPorCargar(false)  
            }


           CambiarGasto(snapshot.docs.map((gasto) =>{

            return {...gasto.data(), id:gasto.id}
           }))
        });

        return unsuscribe;
	
	}, [usuario]);

	return [gasto , obtenerMasgastos, hayMasPorCargar];
}
 
export default useObtenerGasto;