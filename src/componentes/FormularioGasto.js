import React , {useState} from 'react'
import {ContenedorFiltros, Formulario,Input,InputGrande,ContenedorBoton} from '../Elementos/ElementosdeFormulario';
import Boton from '../Elementos/Boton';
import { ReactComponent as IconoPlus } from '../imagenes/plus.svg';
import SelectCategoria from './SelectCategoria';
import DatePicker from './DatePicker';
import AgregarGasto from '../Firebase/AgregarGasto';
import {useAuth} from './../Contexto/AuthContexto'
import { auth } from '../Firebase/firebaseconfig';
import Alerta from '../Elementos/Alerta'


const FormularioGasto = () => {
    const [InputDescripcion , CambiarInputDescripcion]=useState('');
    const [InputCantidad , CambiarInputCantidad]=useState('');
    const [categoria,cambiarCategoria]=useState('hogar');
    const {usuario}=useAuth();
    const [estadoAlerta, cambiarEstadoAlerta] = useState(false);
	const [alerta, cambiarAlerta] = useState({});

    const handleChange=(e)=>{
        if(e.target.name==='Descripcion'){
            CambiarInputDescripcion(e.target.value);
        }else if(e.target.name==='cantidad'){
            CambiarInputCantidad(e.target.value.replace(/[^0-9.]/g , ''));

        }

    }

    const handleSubmit=(e)=>{
         e.preventDefault();
         let cantidad = parseFloat(InputCantidad).toFixed(2);

         //comprobamos valores vacios 
          if(InputDescripcion !== '' && InputCantidad !== ''){

            if(cantidad){

                AgregarGasto({
                    categoria:categoria,
                    descripcion:InputDescripcion,
                    cantidad:cantidad,
                    uidUsuario:usuario.uid
                    //fecha:fecha
                 })
                 .then(() => {
                    cambiarCategoria('hogar');
                    CambiarInputDescripcion('');
                    CambiarInputCantidad('');
                   // cambiarFecha(new Date());

                    cambiarEstadoAlerta(true);
                    cambiarAlerta({tipo: 'exito', mensaje: 'El gasto fue agregado correctamente.'});
                })
                .catch((error) => {
                    cambiarEstadoAlerta(true);
                    cambiarAlerta({tipo: 'error', mensaje: 'Hubo un problema al intentar agregar tu gasto.'});
                })
            }else{
                cambiarEstadoAlerta(true);
            cambiarAlerta({tipo:'error', mensaje:'El valor que ingresate no es correcto'})

            }

          }else{
            cambiarEstadoAlerta(true);
            cambiarAlerta({tipo:'error', mensaje:'Por favor rellena todos los campos'})
          }

        

    }
    return ( 
        <>
        <Formulario onSubmit={handleSubmit}>
            <ContenedorFiltros>
                <SelectCategoria
                categoria={categoria}
                cambiarCategoria={cambiarCategoria}
                />
               <DatePicker/>
            </ContenedorFiltros>
            <div>
              <Input
              type="text"
              name="Descripcion"
              id="Descripcion"
              placeholder="Descripcion"
              value={InputDescripcion}
              onChange={handleChange}

              />
              <InputGrande
               type="text"
               name="cantidad"
               id="cantidad"
               placeholder="Bs0.00"
               value={InputCantidad}
               onChange={handleChange}
 
              
              
              />
            </div>
            <ContenedorBoton>
                <Boton type="submit" as="button" primario conIcono>Agregar Gasto
                <IconoPlus/>
                </Boton>
            </ContenedorBoton>
            <Alerta 
				tipo={alerta.tipo}
				mensaje={alerta.mensaje}
				estadoAlerta={estadoAlerta}
				cambiarEstadoAlerta={cambiarEstadoAlerta}
			/>
        </Formulario>
        {/* <BarraTotalGastado/>*/}
       
        </>
     );
}
 
export default FormularioGasto;
