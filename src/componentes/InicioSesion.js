import React , {useState} from 'react'
import {Helmet} from 'react-helmet'
import {ContenedorHeader,ContenedorBotones,Titulo,Header} from '../Elementos/Header'
import Boton from '../Elementos/Boton'
import {ContenedorFiltros, Formulario,Input,InputGrande,ContenedorBoton} from '../Elementos/ElementosdeFormulario'
import { ReactComponent as Svglogin } from './../imagenes/login.svg'
import styled from 'styled-components'
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min'
import {auth} from './../Firebase/firebaseconfig'
import Alerta from '../Elementos/Alerta'



const Svg=styled(Svglogin)`
   width:100%;
   max-height:12.5rem; /* 200px*/
   margin-bottom:1.25rem;/* 20px*/
`;

const InicioSesion = () => {

  const history=useHistory();
  const [correo, establecercorreo]=useState('');
  const [password, establecerpassword]=useState('');
  const [estadoAlerta,cambiarEstadoAlerta]=useState(false);
  const [alerta , CambiarAlerta]=useState({});


  const handleChange = (e) =>{
    if(e.target.name==='email') {
         establecercorreo(e.target.value);
    }else if(e.target.name==='password'){
          establecerpassword(e.target.value);
    }


  }

  const HandleSubmit = async (e)=>{
    e.preventDefault();
   cambiarEstadoAlerta(false);
   CambiarAlerta({});
    // comprobamos del lado del cliente si el correo es valido

    const expresionregular=/[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+/;

    if(!expresionregular.test(correo)){
     console.log('Ingresar bien tu correo');
     cambiarEstadoAlerta(true);
     CambiarAlerta({
       tipo: 'error',
       mensaje:' Por favor Ingresar un correo electronico Valido'
     });
     return;
   }
     
     if(correo==='' || password ===''){
       console.log("porfavor rellenar los datos");
       cambiarEstadoAlerta(true);
     CambiarAlerta({
       tipo: 'error',
       mensaje:' porfavor rellenar Todos los datos'
     });
      return;
     
       }

       try {
       
         await auth.signInWithEmailAndPassword(correo,password)
         console.log("Usuario Logueado con Exito")
         
        
         history.push('/');
       } catch (error) {
         cambiarEstadoAlerta(true);
         let mensaje;
         switch (error.code) {
           case 'auth/wrong-password':
             mensaje ='La contraseña  no es Correcta.'
             
             break;
             case 'auth/email-already-in-use':
             mensaje ='Ya existe una cuenta con el correo proporcionado.'
             
             break;
             case 'auth/user-not-found':
             mensaje ='No se encontro Ninguna Cuenta con el correo.'
             
             break;
         
           default:
             mensaje ='Hubo un error al crear la cuenta'
             break;
         }
         console.log(mensaje);
         CambiarAlerta({tipo:'error', mensaje:mensaje})
       }

     
   }
  
    return ( 
        <>

      <Helmet>
        <title>Iniciar Sesion</title>
      </Helmet>
       
        <Header>
           <ContenedorHeader>
             <Titulo>Iniciar Sesion</Titulo>
              <div>
                <Boton to ="/crear-cuenta">Crear Cuenta</Boton>
              </div>
           </ContenedorHeader>
           
        </Header>

        <Formulario onSubmit={HandleSubmit}>
            <Svg/>
         <Input
          type="email"
          name="email"
          placeholder='Correo Electronico'
          value={correo}
          onChange={handleChange}
         />

        <Input
          type="password"
          name="password"
          placeholder='Contraseña'
          value={password}
          onChange={handleChange}
         />
         <ContenedorBoton>
          <Boton as="button" primario type="submit">Iniciar Sesion</Boton>
        </ContenedorBoton>
       

        </Formulario>
        <Alerta
          tipo={alerta.tipo}
          mensaje={alerta.mensaje}
          estadoAlerta={estadoAlerta}
          cambiarEstadoAlerta={cambiarEstadoAlerta}
        />
        
      </>
     );
}
 
export default InicioSesion;