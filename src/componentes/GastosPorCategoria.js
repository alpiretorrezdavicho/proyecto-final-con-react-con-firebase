import React from 'react'
import {Helmet} from 'react-helmet'
import  {ContenedorHeader,ContenedorBotones,Titulo,Header} from '../Elementos/Header';
import BtnRegresar from '../Elementos/BtnRegresar';
import BarraTotalGastado from './BarraTotalGastado';

const GastosPorCategorias = () => {
    return ( 

        <>
        <Helmet>
        <title>Gasto por Categoria</title>

        </Helmet>

        <Header>
                <BtnRegresar/>
            <Titulo>Gasto por Categoria</Titulo>

       
        </Header>
        <BarraTotalGastado/>
        
        </>          
        );
}
 
export default GastosPorCategorias;