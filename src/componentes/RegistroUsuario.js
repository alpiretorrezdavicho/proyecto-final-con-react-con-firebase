import React , {useState} from 'react'
import {Helmet} from 'react-helmet'
import {ContenedorHeader,ContenedorBotones,Titulo,Header} from '../Elementos/Header'
import Boton from '../Elementos/Boton'
import {ContenedorFiltros, Formulario,Input,InputGrande,ContenedorBoton} from '../Elementos/ElementosdeFormulario'
import { ReactComponent as Svglogin } from './../imagenes/registro.svg'
import styled from 'styled-components'
import {auth} from './../Firebase/firebaseconfig'
import { useHistory } from 'react-router-dom'
import Alerta from '../Elementos/Alerta'

const Svg=styled(Svglogin)`
   width:100%;
   max-height:6.25rem; /* 100px*/
   margin-bottom:1.25rem;/* 20px*/
`;

const RegistrodeUsuario = () => {

       const history=useHistory();

        const [correo, establecercorreo]=useState('');
        const [password, establecerpassword]=useState('');
        const [password2, establecerpassword2]=useState('');
        const [estadoAlerta,cambiarEstadoAlerta]=useState(false);
        const [alerta , CambiarAlerta]=useState({});
      

        const handleChange = (e) =>{
          switch(e.target.name) {
            case 'email':
                establecercorreo(e.target.value);
              break;
              case 'password':
                establecerpassword(e.target.value);
              break;
              case 'password2':
                establecerpassword2(e.target.value);
              break;
          
            default:
              break;
          }


        }

        const HandleSubmit = async (e)=>{
         e.preventDefault();
        cambiarEstadoAlerta(false);
        CambiarAlerta({});
         // comprobamos del lado del cliente si el correo es valido

         const expresionregular=/[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+/;

         if(!expresionregular.test(correo)){
          console.log('Ingresar bien tu correo');
          cambiarEstadoAlerta(true);
          CambiarAlerta({
            tipo: 'error',
            mensaje:' Por favor Ingresar un correo electronico Valido'
          });
          return;
        }
          
          if(correo==='' || password ==='' || password2===''){
            console.log("porfavor rellenar los datos");
            cambiarEstadoAlerta(true);
          CambiarAlerta({
            tipo: 'error',
            mensaje:' porfavor rellenar Todos los datos'
          });
           return;
          
            }

            if( password !==password2){
              console.log("Las contraseñas No son iguales");
          cambiarEstadoAlerta(true);
          CambiarAlerta({
            tipo: 'error',
            mensaje:' Las contraseñas No son iguales '
          });
              
             return;
            
              }

            try {
              cambiarEstadoAlerta(true);
              CambiarAlerta({
                tipo: 'exito',
                mensaje:' Usuario Registrado Con Exito'
              });
              await auth.createUserWithEmailAndPassword(correo,password)
              console.log("Usuario Registrado con Exito")
              
              history.push('/');
            } catch (error) {
              cambiarEstadoAlerta(true);
              let mensaje;
              switch (error.code) {
                case 'auth/invalid-password':
                  mensaje ='La contraseña  tiene que ser al menos seis Caracteres.'
                  
                  break;
                  case 'auth/email-already-in-use':
                  mensaje ='Ya existe una cuenta con el correo proporcionado.'
                  
                  break;
                  case 'auth/invalid-email':
                  mensaje ='El correo electronico no es valido.'
                  
                  break;
              
                default:
                  mensaje ='Hubo un error al crear la cuenta'
                  break;
              }
              console.log(mensaje);
              CambiarAlerta({tipo:'error', mensaje:mensaje})
            }

        }

    return (

      <>

      <Helmet>
        <title>Crear Cuenta</title>
      </Helmet>
       
        <Header>
           <ContenedorHeader>
             <Titulo>Crear Cuenta</Titulo>
              <div>
                <Boton to ="/iniciar-sesion">Iniciar Sesion</Boton>
              </div>
           </ContenedorHeader>
           
        </Header>

        <Formulario onSubmit={HandleSubmit}>
          <Svg/>
         <Input
          type="email"
          name="email"
          placeholder='Correo Electronico'
          value={correo}
          onChange={handleChange}
         />

        <Input
          type="password"
          name="password"
          placeholder='Contraseña'
          value={password}
          onChange={handleChange}
         />

        <Input
          type="password"
          name="password2"
          placeholder=' Repetir Contraseña'
          value={password2}
          onChange={handleChange}
         />


        <ContenedorBoton>
          <Boton as="button" primario type="submit">Crear Cuenta</Boton>
        </ContenedorBoton>

        </Formulario>
        <Alerta tipo={alerta.tipo} 
        mensaje={alerta.mensaje} 
        estadoAlerta={estadoAlerta}
        cambiarEstadoAlerta={cambiarEstadoAlerta}
        />
      </>

      );
}
 
export default RegistrodeUsuario;