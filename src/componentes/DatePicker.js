import React, { Component } from "react";
import DatePicker, {registerLocale} from 'react-datepicker';
import styled from 'styled-components';
import theme from './../theme';

import 'react-datepicker/dist/react-datepicker.css';
import es from 'date-fns/locale/es';
registerLocale("es", es);


const ContenedorInput = styled.div`
    input {
        font-family: 'Work Sans', sans-serif;       
        box-sizing: border-box;
        background: ${theme.grisClaro};
        border: none;
        cursor: pointer;
        border-radius: 0.625rem; /* 10px */
        height: 5rem; /* 80px */
        width: 100%;
        padding: 0 1.25rem; /* 20px */
        font-size: 1.5rem; /* 24px */
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
    }
 
    @media(max-width: 60rem){ /* 950px */
        & > * {
            width: 100%;
        }
    }
`;

class App extends Component {
state={
  fecha: new Date("2022", "06", "22")
}

onChange=fecha=>{
  this.setState({fecha: fecha});
}



  render() {
    return (
      <ContenedorInput>
        
           <DatePicker selected={this.state.fecha} onChange={this.onChange} locale="es" className="pickers" dateFormat="dd 'de' MMMM 'de' yyyy"/>
         
      </ContenedorInput>
    );
  }
}

export default App;