import React, { Component } from 'react'
import { useAuth } from '../Contexto/AuthContexto';
import { Route , Redirect } from 'react-router-dom/cjs/react-router-dom';

const RutaProtegida = ({children , ...RestosdePropiedades}) => {
    const {usuario}=useAuth();
   
    if(usuario){
        return <Route {...RestosdePropiedades}>{children}</Route>
    }else{

        return <Redirect to='/iniciar-sesion'/>
    }

    
}
 
export default RutaProtegida;