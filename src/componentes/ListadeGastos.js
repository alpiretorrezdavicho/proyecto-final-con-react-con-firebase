import React from 'react'
import {Helmet} from 'react-helmet'
import  {ContenedorHeader,Titulo,Header} from '../Elementos/Header';
import BtnRegresar from '../Elementos/BtnRegresar';
import {useAuth} from '../Contexto/AuthContexto'
import BarraTotalGastado from './BarraTotalGastado';
import useObtenerGasto from '../hook/useObtenerGasto';
import borrarGasto from '../Firebase/borrarGasto';
import {
    Lista,
    ElementoLista,
    ListaDeCategorias,
    ElementoListaCategorias,
    Categoria,
    Descripcion,
    Valor,
    Fecha,
    ContenedorBotones,
    BotonAccion,
    BotonCargarMas,
    ContenedorBotonCentral,
    ContenedorSubtitulo,
    Subtitulo
} from '../Elementos/ElementosDeLista';
import IconoCategoria  from './../Elementos/IconoCategoria';
import { ReactComponent as IconoBorrar } from './../imagenes/borrar.svg';
import { ReactComponent as IconoEditar } from './../imagenes/editar.svg';
import { Link } from 'react-router-dom';
import Boton from '../Elementos/Boton'
const ListadeGastos = () => {

const [gastos, obtenerMasgastos, hayMasPorCargar] =useObtenerGasto();

    return ( 
        <>
        <Helmet>
        <title>Listas de Gastos</title>

        </Helmet>

        <Header>
                <BtnRegresar/>
            <Titulo>Lista de Gastos</Titulo>

       
        </Header>
        <Lista>
            {gastos.map((gasto)=>{
            return (
                <ElementoLista key={gasto.id}>
                    <Categoria>
                    <IconoCategoria id={gasto.categoria}></IconoCategoria>
                     {gasto.categoria}
                    </Categoria>
                    <Descripcion>
                      {gasto.descripcion}
                    </Descripcion>
                    <Valor>{gasto.cantidad}</Valor>
                    <ContenedorBotones>
                        <BotonAccion as={Link} to={`/editar/${gasto.id}`}>
                             <IconoEditar/>
                     </BotonAccion >
                        <BotonAccion onClick={()=>borrarGasto(gasto.id)}>
                            <IconoBorrar/>
                        </BotonAccion>
                    </ContenedorBotones>
                </ElementoLista>
            )
            })}

            {hayMasPorCargar &&
            <ContenedorBotonCentral>
            <BotonCargarMas onClick={()=>obtenerMasgastos()}>Cargar Mas</BotonCargarMas>
           </ContenedorBotonCentral>
            }
            

            {gastos.length===0 && 
                <ContenedorSubtitulo>
                    <Subtitulo>No hay gasto por Mostar</Subtitulo>
                    <Boton as={Link} to="/" >Agregar Gastos</Boton>
                </ContenedorSubtitulo>
            }

        </Lista>
        {/* <BarraTotalGastado/>*/}
        </>      
     );
}
 
export default ListadeGastos;