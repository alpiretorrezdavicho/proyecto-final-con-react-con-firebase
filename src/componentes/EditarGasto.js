import React from 'react'
import {Helmet} from 'react-helmet'
import  {ContenedorHeader,ContenedorBotones,Titulo,Header} from '../Elementos/Header';
import BtnRegresar from '../Elementos/BtnRegresar';
import BarraTotalGastado from './BarraTotalGastado';
import FormularioGasto from './FormularioGasto';
import {useParams} from 'react-router-dom';



const EditarGastos = () => {

   
    return ( 

        
        <>
        <Helmet>
        <title>Editar Gasto</title>

        </Helmet>

        <Header>
                <BtnRegresar/>
            <Titulo>Editar Gasto</Titulo>

       
        </Header>
        <BarraTotalGastado/>
        
        </>          
     );
}
 
export default EditarGastos;