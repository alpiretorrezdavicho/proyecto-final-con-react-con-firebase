import {db} from './firebaseconfig';

const AgregarGasto = ({categoria, descripcion,cantidad,uidUsuario }) => {
    
     return db.collection('gastos').add({
        categoria:categoria,
        descripcion:descripcion,
        cantidad:Number(cantidad),
        uidUsuario:uidUsuario
    

      });
}
 
export default AgregarGasto;