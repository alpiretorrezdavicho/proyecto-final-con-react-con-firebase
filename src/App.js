import React from 'react';
import {Helmet} from 'react-helmet'
import  {ContenedorHeader,ContenedorBotones,Titulo,Header} from './Elementos/Header';
import Boton from './Elementos/Boton'
import BotonCerrarSesion from './Elementos/BotonCerrarSesion';
import FormularioGasto from './componentes/FormularioGasto';

const App = () => {
  return ( 

    <>
    <Helmet>
     <title>Agregar Gasto</title>

    </Helmet>

    <Header>
      <ContenedorHeader>
        <Titulo>Agregar Gasto</Titulo>
        <ContenedorBotones>
           {/*<Boton to="/Categoria">Categorias</Boton>*/}
          
          <Boton to="/lista">Lista Gastos</Boton>
          <BotonCerrarSesion/>
        </ContenedorBotones>
      </ContenedorHeader>
    </Header>
    <FormularioGasto/>
    </>
   );
}
 
export default App;